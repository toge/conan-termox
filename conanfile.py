from conans import ConanFile, CMake, tools


class TermoxConan(ConanFile):
    name = "termox"
    version = "1.1.1"
    license = "MIT"
    author = "toge.mail@gmail.com"
    homepage = "https://github.com/a-n-t-h-o-n-y/TermOx"
    url = "https://bitbucket.org/toge/conan-termox/"
    description = "C++17 Terminal User Interface(TUI) Library."
    topics = ("terminal", "widgets", "tui")
    settings = "os", "compiler", "build_type", "arch"
    options = {"fPIC": [True, False]}
    default_options = {"fPIC": True}
    generators = "cmake"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        self.run("git clone https://github.com/a-n-t-h-o-n-y/TermOx.git")
        self.run("cd TermOx && git checkout v1.1.1 && git submodule update --init --recursive")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="TermOx")
        cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        cmake = CMake(self)
        cmake.configure(source_folder="TermOx")
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["escape", "TermOx"]
